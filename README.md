README
======

This is mostly an exercise to practice with gitlab and to illustrate one potential
application: the collaborative maintenance of notes from any seminar, training
session or whatever.

I considered writing the notes right here, with Markdown language, but I think
it's better to use LaTeX. In this repository I will keep only the source text
and additional files required (images, bibliography...). Then, in your local
version of the repository you can generate the pdf typing the following in your
command line:

    pdflatex BluePippin.tex

When you clone the repository and edit the notes, don't forget to add your name
to the authors line.
