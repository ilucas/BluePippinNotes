\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\title{Notes on the Blue Pippin training session}
\author{J. Ignasi Lucas Lledó}
\begin{document}
\maketitle
\section{Specifications}
\begin{itemize}
  \item There are 5 independent lanes.
  \item Each lane can run a sample of no more than 30 $\mu$l. When the sample volume is lower, it must be diluted with water (not TE, to reduce salt content), to 30 $\mu$l.
  \item The salinity of the sample should never be higher than 40 mM, which is the salinity of the buffer. Higher salt content will modify conductivity and cause unexpected shifts in migration speeds.
  \item PCR or ligation products must be cleaned to remove absolutely all traces of undesired reagents, such as dNTPs or Mg$^{2+}$, which would otherwise alter conductivity as well. To clean the reactions up with magnetic beads is ok.
  \item The maximum amount of DNA that can be loaded per lane is 5 $\mu$g.
  \item The shortest fragment size that can be selected is 250 bp, in order to avoid the contamination of the sample with markers, which are 50 and 150 bp.
\end{itemize}

\section{Description of the cassette}
Cassettes come sealed. It contains the gel and buffer in each lane. Lanes bifurcate near the end of the run, and have three electrodes instead of two. The electrode chambers are full of buffer. There is also an elution well, where the selected fragments will be collected. The volume of the elution well is exactly 40 $\mu$l. A semipermeable membrane separates the elution well from an electrode chamber, to prevent the DNA from passing to the anode chamber.

\section{Principle}
The sensor detects the markers (50 and 150 bp), and using a calibration curve predicts the time at which the desired fragment sizes will reach the bifurcation point. At that time, the anode that was on will switch off, and an alternative anode will switch on to direct the sample towards the elution chamber. As soon as the desired fragment range passed, the current will be restored in the original circuit.

\section{Modes}
The machine can run in three modes.
\subsection{Tight cut mode}
This is the mode to cut the narrowest possible range of fragment sizes. It is used by specifying the center of the range, $c$. The Blue Pippin will select fragments of that size, as tightly as possible. The bounds are usually around the -8\% and +12\%. This is disappointing. It means that the narrowest range is around 20\% of the central size. So, if I want fragments between 600 and 650, I can set the center to 625 and I will get fragments between 575 and 672 (Figure~\ref{fig:tightcut}).

\begin{figure}
\centering{\includegraphics[width=8cm]{images/tightcut.png}}
\caption{Range and actual `center' of the fragment sizes expected from a `tight cut' selection with Blue Pippin. Note that the actual center (red solid line) is expected to be 1.02 times the nominal center (red dashed line).}\label{fig:tightcut}
\end{figure}

\subsection{Range cut mode}
To cut any wider range, use the range cut mode, where you specify the lower and upper bound. If you specify a range as narrow or narrower than the one affordable with a tight cut, the mode is automatically set to tight cut.

\subsection{Peak mode}
This is not available here, because it requires a different cassette, which contains dye. It is used to isolate PCR peaks of uncertain or variable size. The machine figures out what the size of the peak is, and it selects it. This is possible because the DNA gets dyed, in contrast to the two previous modes: regular cassettes do not contain toxic dyes, and only the markers are dyed (but not supposed to be toxic).

\section{Procedure}
\subsection{Program the machine}
The operating system is some flavour of Linux. The machine can be connected to the network, and operated remotely, although it's uncommon. You can start a shell and invoke nautilus to access the graphical interface of the file system.

The program to run the Blue Pippin has four screens or tabs: main, programs, log review\ldots The sample IDs should be recorded in the main tab, which carries the information of the current run. Otherwise, if sample IDs are recorded in the programs screen, they will become available in successive runs, as defaults, which is prone to confusions. In the main tab, you must specify what lanes of the cassette are used, and in what modes. There is also a cassette definition field, which sets the expectations for the calibration step.

You can program a pause within a size range, so that you can split a sample in two contiguous ranges. It is recommended to have the pause in the middle of the range, to get similar yields in both partitions of the sample. The pause is resumed manually during run time, and it should not last more than 15 minutes, to minimize difussion of DNA in the absence of electric potential, which would reduce the precision of the cuts.

\subsection{Air bubbles}
Before opening the cassette, you should make sure it does not contain air bubbles. Look at the light through the cassette. In the gel, air bubbles should not happen, especially around the sensor area. Lanes with air bubbles in the gel should be discarded. Cassettes with more than two lanes affected by air in the gel will be replaced by the company for free.

To remove the air bubbles in the collection (anode) chambers, hold the cassette with the anodes up, and tap it until all bubbles go up. The point is to avoid bubbles from contacting the gel.

Then, you can put the cassette in the machine. Make sure it's in the right position. Unfortunately, it almost fits in the wrong direction. The cassette should sit on the machine so that the light of the sensors is not hitting a sticker with barcodes. Now, you can remove the seal from the cassette, and expose the windows to the collection chambers, the gel, and the elution well.

\subsection{Elution well}
Empty completely the elution well (40 $\mu$l). Refill it with exactly 40 $\mu$l of buffer. You can take the buffer from the stock in the kit or from the collection chamber next to the elution well.

\subsection{Test}
Close the machine and run a test. If the test fails (orange background), run the test again.

\subsection{Loading samples}
The gel bed is not completely full of buffer. First you must add some buffer until it's completely full and the meniscus disappears. You can take some from the cathode chamber. Once you know it has the right amount of liquid, take 40 $\mu$l out, and replace them with the 40 $\mu$l of the mixture of sample (30 $\mu$l) and marker (10 $\mu$l). When loading the sample-marker mixture, make sure you don't poke the gel. Keep the tip of the pippette near the surface. The sample will sink, because the marker is heavy.

It is recommended to seal the cassette with PCR tape. This is to keep the volume in the elution well constant. If some liquid enters the well during the electrophoresis, the tape prevents overflows, and the preasure that builds will make the excess of liquied run through the semipermeable membrane. Sealing the cassette is not essential in tight cut mode, but it does not hurt.

Finally, you can press the `Run' button on the screen. It will take around 1 hour.

\subsection{Log review}
Select the `Last' button in the log review tab to see the report of the run. There are lots of options. Read the \emph{Operations manual} for details.

\subsection{Recover the samples}
Remove the tape. Take the 40 $\mu$l from the elution well. If you did not use all lanes, you can seal the cassette with additional tape in order to re-use it (only unused lanes), within 48 hours.

\section{Maintenance}
\subsection{Cleaning electrodes}
Cleaning the electrodes once per day of use is enough. To clean the electrodes, fill the cleaning cassette with distilled water (all lanes). Insert the cleaning cassette in the machine, close it, and leave it there for a few (10?) minutes. It is extremely important not to forget the cassette inside, because in that case the electrodes would be spoiled (rusted?) irreparably. After you remove the cleaning cassette, you can close the machine. It will dry anyways, and keeping it close prevents dust from accumulating.

You do not need to use new water every time in the cleaning cassette. You can change it once a week.

\subsection{Calibration}
The machine needs to be calibrated once a day of use. There is a fluorescent piece of plastic with a known intensity that is used for calibration. 

\section{The kit}
The kit has marker and buffer. The buffer must be kept at room temperature, whatever the temperature of the room is. The marker must be cool, but not frozen. If it was shipped in ice by mistake and comes frozen, it can be thawed and used, as long as it is not frozen again.

\end{document}
